**EX1**

This project has an algorithmic library to represent a connected, directed and positive graph with weight.
With this library, we can calculate short routes between vertexes in the graph, and particular calculate the shortest path between two vertexes.

This algorithmic library will also be able to give us some statistic calculations :
 calculate the radius, diameter, it can check if the graph upholds the triangle inequality and it able to return the total run time of your queries (ms).

Dijkstra Complexity - O(|E|+|V|log|V|).
See also in Wikipedia: https://en.wikipedia.org/wiki/Dijkstra's_algorithm.
See also in Wikipedia: https://en.wikipedia.org/wiki/Triangle_inequality.

We used here with the library org.jgrapht.alg.
This open source is located at the following link: http://jgrapht.org/javadoc/org/jgrapht/alg/package-summary.html.

You can also run your test here and see some examples in this project on Tests.java.